# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit desktop xdg-utils

MY_PV="${PV}-RC10"

DESCRIPTION="open-world single-player space-combat, roleplaying, exploration and economic game"
HOMEPAGE="http://fractalsoftworks.com/"
SRC_URI="starsector_linux-${MY_PV}.zip"
RESTRICT="fetch test"

LICENSE="starsector"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}
	virtual/jre
	dev-java/icedtea-bin
	media-libs/openal"
BDEPEND=""
#	dev-java/jinput
#	dev-java/log4j-core

S="${WORKDIR}/starsector"

GAMES_DIR="/usr/share/games"

src_prepare() {
	default
	rm -r jre_linux mods saves screenshots
	rm native/linux/libopenal.so  native/linux/libopenal64.so native/linux/libjinput*.so
	rm starsector.*
	sed 's/"allowAnyJavaVersion":false/"allowAnyJavaVersion":true/g' -i data/config/settings.json || die
}

src_install() {
	insinto "${GAMES_DIR}"
	doins -r "${S}"
	exeinto "${GAMES_DIR}/starsector"
	doexe "${FILESDIR}/starsector.sh"
	make_wrapper starsector "${GAMES_DIR}/starsector/starsector.sh"
	newicon -s 64 "graphics/ui/s_icon64.png" "starsector.png"
	newicon -s 32 "graphics/ui/s_icon32.png" "starsector.png"
	newicon -s 16 "graphics/ui/s_icon16.png" "starsector.png"
	make_desktop_entry starsector "Starsector"
}

pkg_postinst() {
	xdg_icon_cache_update
}

pkg_postrm() {
	xdg_icon_cache_update
}

