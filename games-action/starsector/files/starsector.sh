#!/bin/bash

#      _                           _
#  ___| |_ __ _ _ __ ___  ___  ___| |_ ___  _ __
# / __| __/ _` | '__/ __|/ _ \/ __| __/ _ \| '__|
# \__ \ || (_| | |  \__ \  __/ (__| || (_) | |
# |___/\__\__,_|_|  |___/\___|\___|\__\___/|_|
#

XDGDIR=${XDG_CONFIG_HOME:-$HOME/.config}
GAMEDIR="${XDGDIR}/starsector"
INSTDIR="`dirname $0`" ; cd "${INSTDIR}" ; INSTDIR="`pwd`"

[[ ! -d "${XDGDIR}" ]] && mkdir -m 0755 "${XDGDIR}"
[[ ! -d "${GAMEDIR}" ]] && mkdir -m 0755 "${GAMEDIR}"
[[ ! -d "${GAMEDIR}/saves" ]] && mkdir -m 0755 "${GAMEDIR}/saves"
[[ ! -d "${GAMEDIR}/screenshots" ]] && mkdir -m 0755 "${GAMEDIR}/screenshots"
[[ ! -d "${GAMEDIR}/mods" ]] && mkdir -m 0755 "${GAMEDIR}/mods"

JARPATH="janino.jar:commons-compiler.jar:commons-compiler-jdk.jar:starfarer.api.jar:starfarer_obf.jar:jogg-0.0.7.jar:jorbis-0.0.15.jar:json.jar:lwjgl.jar:jinput.jar:log4j-1.2.9.jar:lwjgl_util.jar:fs.sound_obf.jar:fs.common_obf.jar:xstream-1.4.10.jar"

java \
	-server -XX:CompilerThreadPriority=1 \
	-XX:+CompilerThreadHintNoPreempt \
	-Djava.library.path="${INSTDIR}/native/linux" \
	-Xms1536m -Xmx1536m -Xss1024k \
	-classpath "${JARPATH}" \
	-Dcom.fs.starfarer.settings.paths.saves="${GAMEDIR}/saves" \
	-Dcom.fs.starfarer.settings.paths.screenshots="${GAMEDIR}/screenshots" \
	-Dcom.fs.starfarer.settings.paths.mods="${GAMEDIR}/mods" \
	-Dcom.fs.starfarer.settings.paths.logs="${GAMEDIR}" \
	com.fs.starfarer.StarfarerLauncher

exit 0

