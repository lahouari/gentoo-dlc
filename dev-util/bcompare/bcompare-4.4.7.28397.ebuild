# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit rpm desktop xdg-utils

DESCRIPTION="Compare software that allows comparing many file formats in advanced fashion"
HOMEPAGE="https://www.scootersoftware.com"
#HOMEPAGE="https://www.scootersoftware.com/bcompare-4.3.4.24657.x86_64.rpm"
# https://www.scootersoftware.com/files/bcompare-4.4.6.27483.x86_64.rpm
SRC_URI="${HOMEPAGE}/bcompare-${PV}.x86_64.rpm"
RESTRICT="test mirror"

LICENSE="bcompare"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

S=${WORKDIR}

src_unpack() {
	rpm_unpack;
}

src_compile() {
	true
}

src_install() {
	cp -a ${S}/* ${D}
}

pkg_postinst(){
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
}

pkg_postrm(){
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
}
