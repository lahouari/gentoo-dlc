# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

MY_PN="Aseprite"
MY_RCV=1

inherit unpacker xdg-utils

DESCRIPTION="Flash OS images to SD cards & USB drives, safely and easily."
HOMEPAGE="https://www.aseprite.org/"
SRC_URI="https://www.aseprite.org/${MY_PN}_${PV}-${MY_RCV}_amd64.deb"
RESTRICT="mirror fetch"

LICENSE="Proprietary"
SLOT="0"
KEYWORDS="~amd64"

DEPEND=""
RDEPEND=""

S="${WORKDIR}"

src_unpack() {
	unpack_deb ${A}
}

src_install() {
	mv * "${D}" || die
	#rm -rd "${D}/usr/share/doc/balena-etcher-electron"
	#sed -i "s/Utility/System/g" "${D}/usr/share/applications/balena-etcher-electron.desktop"
	#fperms 0755 /opt/balenaEtcher/balena-etcher-electron || die
}

pkg_postinst() {
	xdg_icon_cache_update
	xdg_desktop_database_update
}

pkg_postrm() {
	xdg_icon_cache_update
	xdg_desktop_database_update
}
