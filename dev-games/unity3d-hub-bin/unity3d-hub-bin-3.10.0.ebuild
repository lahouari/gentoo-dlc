# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit unpacker desktop xdg-utils

DESCRIPTION="Unity Hub"
HOMEPAGE="https://unity3d.com"
SRC_URI="https://unity3ddist.jfrog.io/artifactory/hub-debian-prod-local/pool/main/u/unity/unityhub_amd64/unityhub-amd64-${PV}.deb"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

RESTRICT="strip"

S="${WORKDIR}"

src_unpack() {
	unpack_deb ${A}

	gzip "${S}/usr/share/doc/unityhub/changelog.gz" -d
}

src_install() {
	cd ${S}

	insinto /opt/
	doins -r opt/unityhub
	fowners -R root:users /opt/unityhub
	fperms +x /opt/unityhub/unityhub
	fperms +x /opt/unityhub/unityhub-bin
	fperms +x /opt/unityhub/chrome_crashpad_handler
	fperms +x /opt/unityhub/UnityLicensingClient_V1/Unity.Licensing.Client
	fperms +x /opt/unityhub/libEGL.so
	fperms +x /opt/unityhub/libffmpeg.so
	fperms +x /opt/unityhub/libGLESv2.so
	fperms +x /opt/unityhub/libvk_swiftshader.so
	fperms +x /opt/unityhub/libvulkan.so.1
	fperms +x /opt/unityhub/resources/app.asar.unpacked/lib/linux/7z/linux64/7z

	domenu ${FILESDIR}/unityhub.desktop

	insinto /usr/share/icons
	doins -r usr/share/icons

	dodoc usr/share/doc/unityhub/changelog
	# fix documentation folder (version added to the folder name)
	# fix unityhub.desktop TryExec path
}

pkg_postinst() {
	xdg_icon_cache_update
	xdg_desktop_database_update
}

pkg_postrm() {
	xdg_icon_cache_update
	xdg_desktop_database_update
}
