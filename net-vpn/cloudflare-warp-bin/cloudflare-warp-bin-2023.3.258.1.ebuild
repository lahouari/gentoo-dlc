# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit unpacker xdg-utils

DESCRIPTION="Coudflare WARP vpn client"
HOMEPAGE="http://google.com"
SRC_URI="https://pkg.cloudflareclient.com/uploads/cloudflare_warp_2022_10_116_1_amd64_bfe553cbb1.deb"

LICENSE="Cloudflare"
SLOT="0"
KEYWORDS="~amd64"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

RESTRICT="strip"

S=${WORKDIR}

src_unpack(){
	#Unpack deb package
	unpack_deb ${A}
	
	gzip "${S}"/usr/share/doc/cloudflare-warp/changelog.Debian.gz -d
	gzip "${S}"/usr/share/doc/cloudflare-warp/changelog.gz -d
}

src_install(){
	into /
	dobin bin/warp-svc
	dobin bin/warp-cli
	dobin bin/warp-taskbar
	dobin bin/warp-diag

	insinto /usr/share/
	doins -r usr/share/applications
	doins -r usr/share/icons

	insinto /lib/
	doins -r lib/systemd

	insinto /usr/lib/
	doins -r usr/lib/systemd

	dodoc usr/share/doc/cloudflare-warp/changelog.Debian
	dodoc usr/share/doc/cloudflare-warp/changelog
}

pkg_postinst (){
	xdg_desktop_database_update
	xdg_icon_cache_update
}

pkg_postrm (){
	xdg_desktop_database_update
	xdg_icon_cache_update
}
